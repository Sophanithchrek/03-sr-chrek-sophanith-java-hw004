public class DuplicateValueException extends RuntimeException {

    // Constructor: get default data from super class
    public DuplicateValueException() {
        super();
    }

    // Constructor: custom message for output to user
    public DuplicateValueException(String message) {
        super(message);
    }
}
