public class Main {
    public static void main(String[] args) {
        Wrapper<Integer> integerWrapper = new Wrapper<>();
        try {
            integerWrapper.addItem(2);
            integerWrapper.addItem(5);
            integerWrapper.addItem(10);
            integerWrapper.addItem(2);
        } catch (NullValueException | DuplicateValueException ex) {
            System.out.println(ex);
        }

        System.out.println("List all from inputted:");
        for (int i=0; i<integerWrapper.size(); i++) {
            System.out.println(integerWrapper.getItem(i));
        }
    }
}
