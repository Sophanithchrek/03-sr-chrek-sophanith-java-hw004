public class NullValueException extends RuntimeException {

    // Constructor: get default data from super class
    public NullValueException() {
        super();
    }

    // Constructor: custom message for output to user
    public NullValueException(String message) {
        super(message);
    }
}
