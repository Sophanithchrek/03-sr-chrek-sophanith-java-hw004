import java.util.ArrayList;
import java.util.List;

public class Wrapper<E> {
    /*
        - Declare as final list to store item that user has input
        - Declare as final because user purpose only store item, not edit/update/delete
     */
    private final List<E> list = new ArrayList<E>();

    // Declare variable "size" to count item when user has added into list
    private int size = 0;

    // Method to add item to list
    public void addItem(E value) throws NullValueException {
        /**
         * - Check if user input value of data as null value
         * - return message error of exception
         */
        if (value == null)
            throw new NullValueException("Not accept null value!");
        else
            /*
                - Check if user input value that duplicate
                - return message error of exception
             */
            if (list.contains(value))
                throw new DuplicateValueException("Duplicate value : "+value);
            else {
                // If all condition true so user can add item to list
                list.add(value);
                size = size + 1;
            }
    }

    /**
     * - Declare method as "E": Element(Generic Parameter)
     * - Purpose to get item follow by specific index
     */
    public E getItem(int index) {
        return list.get(index);
    }

    // Method to count size of item in list
    public int size() {
        return size;
    }
}
